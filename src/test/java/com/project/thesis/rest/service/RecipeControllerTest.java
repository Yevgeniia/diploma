package com.project.thesis.rest.service;

import com.project.thesis.domain.Recipes;
import com.project.thesis.repositories.RecipeRepository;
import com.project.thesis.services.RecipeService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RecipeController.class)
class RecipeControllerTest {
    @InjectMocks
    RecipeController recipeController;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    RecipeRepository recipeRepository;
    @MockBean
    RecipeService recipeService;
    @MockBean
    Recipes recipes;



    /**
     * Test is done
     *
     * @throws Exception
     */
    @Test
    public void recipesViewTest() throws Exception {

        this.mockMvc.perform(get("/recipe"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe"))
                .andDo(print());

    }

    /**
     * Test is done
     * URL is being redirected after entering Request parameter using Input
     *
     * @throws Exception
     */
    @Test
    public void searchTest() throws Exception {

        this.mockMvc.perform(get("/recipe/search"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlTemplate("/recipe/search/null"))
                .andDo(print());

    }

    /**
     * Test is done
     *
     * @throws Exception
     */
    @Test
    public void searchByParameterTest() throws Exception {
        this.mockMvc.perform(get("/recipe/search/{title}", "helloTitle"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    /**
     * Test is done
     * Restriction for unauthorized users
     *
     * @throws Exception
     */
    @Test
    public void addGetRequestTest() throws Exception {

        this.mockMvc.perform(get("/recipe/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipeAdd"))

                .andDo(print());
    }


    /**
     * Test is done
     * Error - was returned 302 status, which was added to controller to resolve the error
     *
     * @throws Exception
     */
    @Test
    //by id and author
    public void updateRecordTest() throws Exception {
        Recipes resNew = new Recipes();
        resNew.setId(1L);
        resNew.setPretext("pree");
        resNew.setTitle("Title");
        resNew.setAuthor("Eva");

        this.mockMvc.perform(get("/recipe/{id}/{author}/update", resNew.getId(), resNew.getAuthor()))
                .andDo(print())
                .andExpect(status().isOk());


    }




}