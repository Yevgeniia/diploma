package com.project.thesis.rest.service;

import com.project.thesis.domain.AppUser;
import com.project.thesis.domain.Recipes;
import com.project.thesis.services.MainService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(MainController.class)
class MainControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @InjectMocks
    MainController mainController;
    @MockBean
    MainService mainService;
    @Autowired
    private WebApplicationContext wac;


    @Autowired
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(mainController);

    }


    /**
     * Test returns home page view
     *
     * @throws Exception in case something went wrong during execution of the program
     */
    @Test
    public void homePageTest() throws Exception {
        Recipes recipes = new Recipes();
        recipes.setId(null);
        recipes.setTitle("ff");
        recipes.setAuthor("eva");
        recipes.setPretext("dd");
        recipes.setFull_text("ff");


        this.mockMvc.perform(get("/")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("pages"))
                .andExpect(view().name("home"));

    }

    /**
     * Returns register form page
     *
     * @throws Exception in case something went wrong during execution of the program
     */
    @Test
    public void registerGet() throws Exception {

        this.mockMvc.perform(get("/register"))
                .andDo(print())
                .andExpect(status().isOk());


    }

    /**
     * User is created flash attribute
     *
     * @throws Exception in case something went wrong during execution of the program
     */
    @Test
    public void registerPost() throws Exception {
        AppUser user = new AppUser();
        user.setId(null);
        user.setUsername("Eva");
        user.setPassword("123");
        user.setPassword(encoder().encode("123"));


        this.mockMvc.perform(post("/register").param("username", user.getUsername())
                .param("password", user.getPassword()))
                .andExpect(status().isFound())
                .andDo(print());

    }

    /**
     * Return AboutUs view
     *
     * @throws Exception
     */
    @Test
    public void aboutUsTest() throws Exception {
        this.mockMvc.perform(get("/aboutUs"))
                .andExpect(status().isOk())
                .andExpect(view().name("aboutUs"))
                .andDo(print());

    }


}