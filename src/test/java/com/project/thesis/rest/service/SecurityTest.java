package com.project.thesis.rest.service;


import com.project.thesis.domain.AppUser;
import com.project.thesis.domain.Role;
import com.project.thesis.repositories.AppUserRep;
import com.project.thesis.repositories.RoleRep;
import com.project.thesis.services.UserDetailServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SecurityTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    AppUserRep userRepository;
    @MockBean
    UserDetailServiceImp userDetailServiceImp;
    @MockBean
    RoleRep roleRep;
    @MockBean
    UserDetails userDetails;

    @Autowired
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    /**
     * Get login page view
     *
     * @throws Exception
     */
    @Test
    public void loginPage() throws Exception {
        mockMvc.perform(get("/loginPage")).andExpect(status().isOk());
    }

    /**
     * Login with User Details
     *
     * @throws Exception
     */
    @Test
    public void formLogin() throws Exception {

        Role role = new Role();
        role.setRoleId(1L);
        role.setRoleName("USER");

        when(roleRep.findByRoleName(role.getRoleName())).thenReturn(role);

        Role roleName = roleRep.findByRoleName(role.getRoleName());

        AppUser user = new AppUser();
        user.setId(1L);
        user.setUsername("Eva");
        user.setPassword(encoder().encode("123"));
        user.setRoles(new HashSet<>(Arrays.asList(roleName)));

        UserDetails userDetails = new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                for (Role role : user.getRoles()) {
                    authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
                }
                return authorities;
            }

            @Override
            public String getPassword() {
                return user.getPassword();
            }

            @Override
            public String getUsername() {
                return user.getUsername();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
        mockMvc
                .perform(get("/").with(user(userDetails))).andExpect(status().isOk());
    }

    /**
     * Redirection to loginPage after logout
     *
     * @throws Exception
     */
    @Test
    public void logout() throws Exception {
        mockMvc.perform(get("/logout"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/loginPage?logout"));

    }

    /**
     * In case of login error
     *
     * @throws Exception
     */
    @Test
    public void loginError() throws Exception {
        mockMvc.perform(get("/login-error"))
                .andExpect(view().name("login"))
                .andExpect(model().attribute("loginError", true));

    }


}
