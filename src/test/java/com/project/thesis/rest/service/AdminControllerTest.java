package com.project.thesis.rest.service;

import com.project.thesis.repositories.RecipeRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlTemplate;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AdminController.class)
class AdminControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AdminController adminController;

    @MockBean
    RecipeRepository recipeRepository;



    @Test
    void recipePostDelete() throws Exception {

        this.mockMvc.perform(post("/admin/1/delete"))
                .andExpect(redirectedUrlTemplate("http://localhost/loginPage"))
                .andExpect(status().is3xxRedirection())
                .andDo(print());
    }

    @Test
    void update() throws Exception {

        this.mockMvc.perform(post("/admin/1/update"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlTemplate("http://localhost/loginPage"))
                .andDo(print());
    }

    @Test
    void updateAdmin() throws Exception {

        this.mockMvc.perform(get("/admin/1/update"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlTemplate("http://localhost/loginPage"))
                .andDo(print());
    }
}