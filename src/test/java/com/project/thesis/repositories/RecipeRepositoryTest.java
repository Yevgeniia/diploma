package com.project.thesis.repositories;

import com.project.thesis.domain.Recipes;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasProperty.hasProperty;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class RecipeRepositoryTest {

    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    TestEntityManager testEntityManager;

    @Test
    void getUserIdForPage() {
      Recipes recipe1 = new Recipes();

        List<Recipes> found = recipeRepository.getUserIdForPage(recipe1.getId());

        assertEquals(2, found.size());
    }

    @Test
    void findByAuthor() {
        Recipes recipes = new Recipes();
        recipes.setTitle("Title");
        recipes.setFull_text("Full");
        recipes.setPretext("Text");
        recipes.setAuthor("Eva");

        testEntityManager.persist(recipes);
        testEntityManager.flush();

        List<Recipes> r = recipeRepository.findByAuthor("Eva");

        assertEquals(1, r.size());
        assertNotNull(r);
        assertTrue(StringUtils.isNotBlank(recipes.getTitle()));
        assertTrue(StringUtils.isNotBlank(recipes.getFull_text()));
        assertTrue(StringUtils.isNotBlank(recipes.getPretext()));
        assertTrue(StringUtils.isNotBlank(recipes.getAuthor()));


    }

    @Test
    void saveRecipe() {
        Recipes recipes = new Recipes();
        recipes.setTitle("New title");
        recipes.setFull_text("FullText");
        recipes.setPretext("Pretext pretext");
        recipes.setAuthor("Eva");

        testEntityManager.persist(recipes);
        testEntityManager.flush();

        Recipes saved = recipeRepository.save(recipes);

        assertEquals(saved, recipes);
        assertTrue(StringUtils.isNotBlank(recipes.getTitle()));
        assertTrue(StringUtils.isNotBlank(recipes.getFull_text()));
        assertTrue(StringUtils.isNotBlank(recipes.getAuthor()));
        assertTrue(StringUtils.isNotBlank(recipes.getPretext()));

    }


    @Test
    void findByTitle() {


        Recipes recipes = new Recipes();
        recipes.setTitle("Title");
        recipes.setFull_text("Full");
        recipes.setPretext("Text");
        recipes.setAuthor("Eva");

        testEntityManager.persist(recipes);
        testEntityManager.flush();

        List<Recipes> r = recipeRepository.findByTitle("Title");

        assertEquals(1, r.size());

        assertThat(recipes, hasProperty("title"));
        assertThat(recipes, hasProperty("pretext"));
        assertTrue(StringUtils.isNotBlank(recipes.getTitle()));
        assertTrue(StringUtils.isNotBlank(recipes.getFull_text()));
        assertTrue(StringUtils.isNotBlank(recipes.getPretext()));
        assertTrue(StringUtils.isNotBlank(recipes.getAuthor()));

    }
}