package com.project.thesis.repositories;

import com.project.thesis.domain.AppUser;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
class AppUserRepTest {
    @Autowired
    private AppUserRep appUserRep;
    @Autowired
    TestEntityManager testEntityManager;


    @Test
    void findByUsername() {
        AppUser user = new AppUser();
        user.setUsername("Eva");
        user.setPassword("123");


        testEntityManager.persist(user);
        testEntityManager.flush();

        AppUser findUser = appUserRep.findByUsername("Eva");

        assertEquals(user, findUser);
        assertTrue(StringUtils.isNotBlank(user.getUsername()));
        assertTrue(StringUtils.isNotBlank(user.getPassword()));
    }

    @Test
    void existsByNameTest(){
        AppUser user = new AppUser();
        user.setUsername("Eva");
        user.setPassword("123");

        testEntityManager.persist(user);
        testEntityManager.flush();

        Boolean findUser = appUserRep.existsByUsername("Eva");
        assertTrue(findUser);
    }
}