package com.project.thesis.repositories;

import com.project.thesis.domain.Role;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
class RoleRepTest {

    @Autowired
    private RoleRep roleRep;
    @Autowired
    TestEntityManager testEntityManager;



    @Test
    void findByRoleName() {
        Role role = new Role();
        role.setRoleId(1L);
        role.setRoleName("ADMIN");

        Role found = roleRep.findByRoleName("ADMIN");

        assertEquals(role, found);
        assertTrue(StringUtils.isNotBlank(role.getRoleName()));


    }
}