package com.project.thesis.rest.service;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Controller class for handling web requests
 * ErrorController for providing a custom whitelabel error page.
 */
@Controller
public class MyErrorController implements ErrorController {

    /**
     * @param request parameter of HttpServletRequest interface
     * @return error view based on its HTTP status
     */
    @RequestMapping("/error")
    public String errorKeeper(HttpServletRequest request) {
        // Getting error status code
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            Integer statusValue = Integer.valueOf(status.toString());
            if (statusValue == HttpStatus.NOT_FOUND.value()) {
                return "404";
            } else if (statusValue == HttpStatus.FORBIDDEN.value()) {
                return "403";
            } else if (statusValue == HttpStatus.NO_CONTENT.value()) {
                return "204";
            } else if (statusValue == HttpStatus.CONFLICT.value()) {
                return "409";
            }
        }
        //Return basin error view in case if httpStatus is any other except 404,403,204,409
        return "error";
    }

    /**
     * Method from Error Controller interface
     *
     * @return null, because instead "server.error.path" in application.properties is being used
     */
    @Override
    public String getErrorPath() {
        return null;

    }
}
