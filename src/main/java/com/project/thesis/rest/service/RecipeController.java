package com.project.thesis.rest.service;


import com.project.thesis.domain.Recipes;
import com.project.thesis.services.RecipeService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;


@Controller
@RequestMapping("/recipe")
public class RecipeController {

    RecipeService recipeService;
    /**
     * Dependency injection
     */
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    /**
     * HTTP get request for Recipe view
     *
     * @param model holder for model attributes
     * @return recipe view with representation of all records from recipe table in database
     */
    @GetMapping()
    public String recipe(Model model) {
        //get all notes from recipes table
        Iterable<Recipes> recipes = recipeService.findAll();
        model.addAttribute("recipes", recipes);
        return "recipe";
    }

    /**
     * HTTP get request of URL '/search/{parameter}
     *
     * @param searchTitle parameter to extract searchTitle parameter from user's input
     * @return url of '/search/{parameter}'
     */
    @GetMapping(value = "/search")
    public String searchUrl(@RequestParam(value = "searchTitle", required = false) String searchTitle) {
        return "redirect:/recipe/search/" + searchTitle;

    }

    /**
     * HTTP get request
     *
     * @param model     holder for model attributes
     * @param parameter for handling variable in request URI mapping
     * @return recipes found by title view
     */
    @GetMapping("/search/{parameter}")
    public String search(Model model, @PathVariable("parameter") String parameter, HttpServletResponse response) {

        List<Recipes> recipes = recipeService.findByTitle(parameter);
        if (recipes.isEmpty()) {
            return String.valueOf(HttpStatus.NO_CONTENT.value());
        }
        model.addAttribute("search", recipes);
        return "searchList";
    }

    /**
     * HTTP get request for '/recipe/add' URL
     *
     * @param model holder for model attributes
     * @return recipe add form view
     */
    @GetMapping("/add")
    public String recipeAdd(Model model) {
        model.addAttribute("recipe", new Recipes());
        return "recipeAdd";

    }


    /**
     * HTTP post request for processing recipe add functionality
     *
     * @param recipes   parameter of Recipes entity with @Valid annotation for validation the annotated fields
     * @param result    object which holds the result of validation and binding
     * @param principal parameter of currently logged user
     * @return recipe view if recipe added successfully
     */

    @PostMapping("/add")
    public String recipePostAdd(@ModelAttribute("recipe") @Valid Recipes recipes, BindingResult result,
                                @RequestParam String title,
                                @RequestParam String pretext,
                                @RequestParam String full_text,
                                Principal principal) {
        //Return add recipe view with alert messages in case binding result contains errors
        if (result.hasErrors()) {
            return "recipeAdd";
        }
        //add to database
        recipeService.addRecipe(title, pretext, full_text, principal);
        return "redirect:/recipe";
    }

    /**
     * HTTP get request
     *
     * @param id     for handling id variable in request URI mapping
     * @param author for handling author variable in request URI mapping
     * @param model  holder for model attributes
     * @return details of recipe view
     */
    @GetMapping("/{id}/{author}")
    public String recipeDetails(@PathVariable(value = "id") Long id, @PathVariable(value = "author") String author,
                                Model model) {
        List<Recipes> auth = recipeService.findByAuthor(author);

        Optional<Recipes> recipeGet = recipeService.findById(id);
        Recipes recipes = recipeGet.get();
        model.addAttribute("recipe", recipes);
        model.addAttribute("auth", auth);
        return "recipeDetails";
    }

    /**
     * HTTP get request
     *
     * @param id     for handling id variable in request URI mapping
     * @param author for handling author variable in request URI mapping
     * @param model  holder for model attributes
     * @return recipe edit view
     */
    @GetMapping("/{id}/{author}/update")
    public String edit(@PathVariable(value = "id") Long id, @PathVariable("author") String author, Model model) {


        Optional<Recipes> recipeUpdate = recipeService.findById(id);

        if (recipeUpdate.isPresent()) {
            Recipes recipes = recipeUpdate.get();
            model.addAttribute("updateRecipe", recipes);
            return "edit";
        } else {
            return String.valueOf(HttpStatus.NOT_FOUND.value());
        }
    }


    /**
     * HTTP post request
     *
     * @param recipes   parameter of Recipes entity with @Valid annotation for validation the annotated fields
     * @param result    object which holds the result of validation and binding
     * @param id        for handling id variable in request URI mapping
     * @param title     parameter for binding web request 'title' parameter to 'setTitle' method parameter
     * @param pretext   parameter for binding web request 'pretext' parameter to 'setPretext' method parameter
     * @param full_text parameter for binding web request 'full_text' parameter to 'setFull_text' method parameter
     * @param author    for handling author variable in request URI mapping
     * @param principal parameter of currently logged user
     * @return recipe  view if record is being updated
     */
    @PostMapping("/{id}/{author}/update")
    public String recipePostUpdate(@ModelAttribute("updateRecipe") @Valid Recipes recipes, BindingResult result,
                                   @PathVariable(value = "id") Long id,
                                   @RequestParam String title, @RequestParam String pretext,
                                   @RequestParam String full_text, @PathVariable("author") String author, Principal principal
    ) {
        //Return edit view with alert messages if binding result contained errors
        if (result.hasErrors()) {
            return "edit";
        }
        //Check if currently logged user is equal to author of record
        if (principal.getName().equals(author)) {
            recipeService.update(title, pretext, full_text, principal, recipes);
        } else {
            //in case currently logged user is not equal to author of record, return noPermission view
            return "noPermission";
        }

        return "redirect:/recipe";
    }


}
