package com.project.thesis.rest.service;

import com.project.thesis.domain.AppUser;
import com.project.thesis.domain.Recipes;
import com.project.thesis.services.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * Controller class for handling web requests
 */

@Controller
public class MainController {
    /**
     * Dependency injections
     */
    @Autowired
    MainService mainService;

    /**
     * HTTP get request for Home page view
     *
     * @param id    parameter with @Param annotation for binding query from repository with id parameter
     * @param model holder for model attributes
     * @return home page view
     */
    @GetMapping("/")
    public String home(Long id, Model model) {
        List<Recipes> recipes = mainService.lastTwoRecords(id);
        //Pass model attribute
        model.addAttribute("pages", recipes);
        return "home";
    }

    /**
     * HTTP get request for '/register' URL
     *
     * @param model holder for model attributes
     * @return registration page view
     */
    @GetMapping("/register")
    public String registerForm(Model model) {
        AppUser user = new AppUser();
        model.addAttribute("registerUser", user);
        return "register";
    }

    /**
     * User registration controller
     *
     * @param user               parameter for validation constrains of user entity
     * @param result             parameter for binding result, in case validation failed
     * @param username           parameter for User's name to be saved
     * @param password           password to be saved
     * @param redirectAttributes flash attribute in case user is created
     * @return register page, in case registration is successful
     */
    @PostMapping("/register")
    public String register(@ModelAttribute("registerUser") @Valid AppUser user, BindingResult result,
                           @RequestParam String username,
                           @RequestParam String password, RedirectAttributes redirectAttributes) {


        if (result.hasErrors()) {
            return "register";
        }

        boolean userExists = mainService.userExists(username);

        if (userExists) {
            return String.valueOf(HttpStatus.CONFLICT.value());
        } else {
            redirectAttributes.addFlashAttribute("created", "User is created!");
            mainService.registerUser(username, password);
        }
        return "redirect:/register?success";
    }


    /**
     * HTTP get request for login page URL
     *
     * @return login page view after entering login page URL
     */
    @GetMapping("/loginPage")
    public String home() {

        return "login";
    }

    /**
     * HTTP get request for error page, in case authorization is not completed
     *
     * @param model holder for model attributes
     * @return login page view with error message
     */
    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    /**
     * HTTP get request for logout method
     *
     * @param request  parameter of HttpServletRequest interface
     * @param response parameter of HttpServletResponse interface
     * @return login page view in case logout method is being processed
     */
    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            new SecurityContextLogoutHandler().logout(request, response, null);
        }
        return "redirect:/loginPage";
    }

    /**
     * HTTP get request for about us URL
     *
     * @param model holder for model attributes
     * @return about us page view
     */
    @GetMapping("/aboutUs")
    public String about(Model model) {
        model.addAttribute("about", "about page");
        return "aboutUs";
    }


}