package com.project.thesis.rest.service;

import com.project.thesis.domain.Recipes;
import com.project.thesis.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller class for handling web requests
 */
@Controller
/**
 * Handling URLs starting from /admin
 */
@RequestMapping("/admin")
public class AdminController {

    /**
     * Dependency injection
     */
    @Autowired
    AdminService adminService;

    /**
     * HTTP post request for deleting a record from database
     *
     * @param id parameters of record to be deleted by id
     * @return /recipe page in case record was deleted
     */
    @PostMapping("/{id}/delete")
    public String recipePostDelete(@ModelAttribute("delete") @PathVariable(value = "id") Long id) {

        adminService.recipeDelete(id);

        return "redirect:/recipe";
    }


    /**
     * HTTP get method for getting URL to update method
     *
     * @param id    path variable in URL
     * @param model holder for model attributes
     * @return view for editing a record
     */
    @GetMapping("/{id}/update")
    public String update(@PathVariable(value = "id") Long id, Model model) {
        Recipes recipes = adminService.findById(id);
        if (recipes != null) {
            model.addAttribute("updateAdmin", recipes);
            return "updateAdmin";
        } else {
            return String.valueOf(HttpStatus.NOT_FOUND.value());
        }
    }

    /**
     * HTTP post method for processing CRUD update method
     *
     * @param recipes parameter of Recipes entity with @Valid annotation for validation the annotated fields
     * @param result  object which holds the result of validation and binding
     * @param title   parameter for binding web request 'title' parameter to 'setTitle' method parameter
     * @param pretext parameter for binding web request 'pretext' parameter to 'setPretext' method parameter
     * @param full    parameter for binding web request 'full' parameter to setFull_text method parameter
     * @return recipes page view in case the record was updated successfully
     */
    @PostMapping("/{id}/update")
    public String updateAdmin(@ModelAttribute("updateAdmin") @Valid Recipes recipes, BindingResult result,
                              @RequestParam("title") String title,
                              @RequestParam("pretext") String pretext, @RequestParam("full_text") String full) {
        //If binding result has error return view with alert notes
        if (result.hasErrors()) {
            return "edit";
        }
        adminService.update(title, pretext, full,recipes);
        return "redirect:/recipe";
    }

}
