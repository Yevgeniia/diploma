package com.project.thesis.services;

import com.project.thesis.domain.Recipes;
import com.project.thesis.repositories.RecipeRepository;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    RecipeRepository recipeRepository;

    /**
     * Dependency injection
     * @param recipeRepository repository instance
     */
    public AdminService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * Get recipe by Id and delete
     * @param id of recipe, which has to be deleted
     */
    public void recipeDelete(Long id) {
        Recipes recipes = recipeRepository.findById(id).orElseThrow();
        recipeRepository.delete(recipes);
    }

    /**
     * Find recipe by Id
     * @param id of recipe, which has to be found
     * @return recipe by id
     */
    public Recipes findById(Long id) {
        Recipes recipes = recipeRepository.findById(id).orElseThrow();
        return recipes;
    }

    /**
     *
     * @param title parameter for title to update
     * @param pretext parameter for pretext to update
     * @param full parameter for full text to update
     */
    public Recipes update(String title, String pretext, String full,Recipes recipes) {
        recipes.setTitle(title);
        recipes.setPretext(pretext);
        recipes.setFull_text(full);
        recipes.setLastModificationDate(recipes.getLastModificationDate());
        recipeRepository.save(recipes);
        return recipes;
    }
}
