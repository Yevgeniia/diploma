package com.project.thesis.services;

import com.project.thesis.domain.Recipes;
import com.project.thesis.repositories.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {
    private RecipeRepository recipeRepository;

    /**
     * Dependency injection
     * @param recipeRepository
     */
    @Autowired
    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     *
     * @return all recipes from database
     */
    public Iterable<Recipes> findAll() {
        Iterable<Recipes> recipes = recipeRepository.findAll();
        return recipes;
    }

    /**
     *
     * @param title parameter for finding recipe by title
     * @return list of recipes found by title
     */
    public List<Recipes> findByTitle(String title) {
        List<Recipes> recipes = recipeRepository.findByTitle(title);
        return recipes;
    }

    /**
     *
     * @param title parameter of recipe to be added
     * @param pretext parameter of recipe to be added
     * @param full_text parameter of recipe to be added
     * @param principal currently logged in user
     * @return saved recipe
     */
    public Recipes addRecipe(String title, String pretext, String full_text, Principal principal) {
        Recipes recipes = new Recipes();
        recipes.setTitle(title);
        recipes.setPretext(pretext);
        recipes.setFull_text(full_text);
        recipes.setAuthor(principal.getName());
        recipes.setCreationDate(new Date());
        //add to database
        recipeRepository.save(recipes);
        return recipes;
    }

    /**
     *
     * @param title parameter of recipe to be updated
     * @param pretext parameter of recipe to be updated
     * @param full_text parameter of recipe to be updated
     * @param principal currently logged in user
     * @param recipes parameter of recipe to be saved
     * @return updated record
     */
    public Recipes update(String title, String pretext, String full_text, Principal principal, Recipes recipes) {
        recipes.setTitle(title);
        recipes.setPretext(pretext);
        recipes.setFull_text(full_text);
        recipes.setAuthor(principal.getName());
        recipes.setCreationDate(new Date());
        //add to database
        recipeRepository.save(recipes);
        return recipes;
    }

    /**
     *
     * @param auth parameter for author
     * @return list of records found by author
     */
    public List<Recipes> findByAuthor(String auth) {
        List<Recipes> byAuthor = recipeRepository
                .findByAuthor(auth);
        return byAuthor;
    }

    /**
     *
     * @param id parameter for recipe's id to be found
     * @return optional of recipes found by id
     */
    public Optional<Recipes> findById(Long id) {
        Optional<Recipes> byId = recipeRepository
                .findById(id);
        return byId;
    }
}
