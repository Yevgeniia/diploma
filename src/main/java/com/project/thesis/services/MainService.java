package com.project.thesis.services;

import com.project.thesis.domain.AppUser;
import com.project.thesis.domain.Recipes;
import com.project.thesis.domain.Role;
import com.project.thesis.repositories.AppUserRep;
import com.project.thesis.repositories.RecipeRepository;
import com.project.thesis.repositories.RoleRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class MainService {
    /**
     * Method for password encoding
     *
     * @return bcrypt encoder
     */
    @Autowired
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    RecipeRepository recipeRepository;
    AppUserRep userRep;
    RoleRep roleRep;

    /**
     * Dependency injection by constructor
     * @param recipeRepository
     * @param userRep
     * @param roleRep
     */
    public MainService(RecipeRepository recipeRepository, AppUserRep userRep, RoleRep roleRep) {
        this.recipeRepository = recipeRepository;
        this.userRep = userRep;
        this.roleRep = roleRep;
    }

    /**
     *
     * @param id parameter of id of last two records in database
     * @return list of two last recipes in database
     */
    public List<Recipes> lastTwoRecords(Long id) {
        List<Recipes> recipes = recipeRepository.getUserIdForPage(id);
        return recipes;
    }

    /**
     * Check is user by username is preset
     * @param username parameter for username which has to be checked
     * @return boolean value
     */
    public boolean userExists(String username) {
        boolean userPresent = userRep.existsByUsername(username);
        return userPresent;
    }

    /**
     *
     * @param username parameter of username to be registered
     * @param password password of user
     * @return registered user
     */
    public AppUser registerUser(String username, String password) {
        AppUser user = new AppUser();
        user.setUsername(username);
        user.setPassword(encoder().encode(password));
        Role role = roleRep.findByRoleName("USER");
        user.setCreationDate(user.getCreationDate());
        user.setRoles(new HashSet<>(Arrays.asList(role)));
        userRep.save(user);
        return user;
    }
}
