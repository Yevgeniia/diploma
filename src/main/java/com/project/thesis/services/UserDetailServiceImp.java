package com.project.thesis.services;

import com.project.thesis.domain.AppUser;
import com.project.thesis.repositories.AppUserRep;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Getting user related data for providing authentication of an application
 */
@Service("userDetailsService")
public class UserDetailServiceImp implements UserDetailsService {

    /**
     * Dependency injection
     */
    AppUserRep userRep;

    public UserDetailServiceImp(AppUserRep userRep) {
        this.userRep = userRep;
    }

    /**
     * Method to find user by username in db
     *
     * @param username parameter
     * @return User Detail instance with user core information
     * @throws UsernameNotFoundException exception in case User name is not in database
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        AppUser appUser = userRep.findByUsername(username);
        if (appUser == null) {
            throw new UsernameNotFoundException("Not found");
        }

        return appUser;
    }

}
