package com.project.thesis.repositories;

import com.project.thesis.domain.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRep extends CrudRepository<Role, Long> {
    /**
     * Method for retrieving role in table 'roles' by its name
     * @param name parameter for name of role
     * @return role instance by its name
     */
    Role findByRoleName(String name);
}
