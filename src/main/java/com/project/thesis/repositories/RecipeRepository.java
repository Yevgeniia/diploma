package com.project.thesis.repositories;

import com.project.thesis.domain.Recipes;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RecipeRepository extends CrudRepository<Recipes, Long> {
    /**
     * Method getUserIdForPage for representing last to elements in 'recipes' table
     * @param id parameter for finding last two elements in database
     * @return two last element in database from the end of table
     */
    @Query(nativeQuery = true, value = "SELECT * FROM recipes ORDER BY id DESC LIMIT 2")
    List<Recipes> getUserIdForPage(@Param("id") Long id);



    /**
     * Method for retrieving recipe from database by 'Author'
     * @param author parameter of column 'author'
     * @return recipe in 'recipes' table by its author
     */
    List<Recipes> findByAuthor(@Param("author") String author);

    /**
     * Method for finding recipe in Recipes table by title. Concatenation used for titles which
     * have more than one world in title with spacing
     * @param title parameter for column title in 'recipes' table
     * @return recipe found by title in "Recipes" table
     */
    @Query(nativeQuery = true, value = "SELECT * FROM recipes r WHERE r.title LIKE CONCAT('%',?, '%')")
    List<Recipes> findByTitle(@Param("title") String title);





}
