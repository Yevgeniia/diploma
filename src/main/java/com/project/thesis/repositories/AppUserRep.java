package com.project.thesis.repositories;

import com.project.thesis.domain.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRep extends CrudRepository<AppUser, Long> {

    /**
     * Method for retrieving user by userName from database
     * @param username parameter of user name
     * @return user by his name, if it exists in database
     */
    AppUser findByUsername(String username);

    /**
     * Method for checking existence of a user in database
     * @param username parameter for user's name
     * @return boolean true or false depending on user existence in database
     */
    boolean existsByUsername(String username);




}
