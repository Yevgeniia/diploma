package com.project.thesis.domain;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor @Getter @Setter
@Entity
@Data
public class Recipes extends AbstractDomain {
    /**
     * Recipes table columns with validation specifications
     */
    @Column
    @NotEmpty(message = "Field can not be empty")
    @Size(min = 2, message = "The size of the field should be at least 2 characters long!")
    private String title, pretext, full_text;
    @Column
    private String author;

}
