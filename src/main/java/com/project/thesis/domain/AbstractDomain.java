package com.project.thesis.domain;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

import java.util.Date;

/**
 * Abstract domain class for providing last modification date and creation date
 * for any entity class of application
 */
@Getter
@Setter
@Data
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractDomain {
    /**
     * Required for every entity id column, which generates automatically
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * @Temporal - JPA annotation for storing Timestamp(in this application) item
     */
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationDate;


}
