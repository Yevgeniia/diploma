package com.project.thesis.domain;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * UserDetails providers user core information
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
public class AppUser extends AbstractDomain implements UserDetails {
    /**
     * Columns in AppUser table in database with validation specifications
     */
    @Column(unique = true)
    @NotEmpty(message = "Username can not be empty")
    @Size(min = 3, message = "Username should be at least 3 characters long")
    private String username;
    @NotEmpty(message = "Field password can not be empty")
    @Size(min = 3, message = "Password should be at least 3 characters long")
    private String password;

    /**
     * Relationship between two tables
     */
    @ManyToMany(cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
    Set<Role> roles = new HashSet<>();

    /**
     * @return authorities granted to the user, taken from Roles table in database
     * Table Roles contains two roles - USER and ADMIN
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (Role role : getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        }
        return authorities;

    }

    /**
     * @return boolean true that user account data has no expiration
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * @return boolean true information about user account is not being locked
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * @return boolean true that user password has no expiration
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * @return true that user account is enabled
     */
    @Override
    public boolean isEnabled() {
        return true;
    }


}
