package com.project.thesis.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * WebSecurityConfigurerAdapter enables HTTP security in Spring application
 * It provides configure() methods
 */
@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Dependency injection
     */
    @Autowired
    UserDetailsService userDetailsService;

    /**
     * Method for encrypting the password of User for securing the application
     *
     * @return bcrypt encoder for storing password to database
     */

    @Autowired
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Method to establish authentication with UserDetailsService
     * UserDetailsService is used to get user-related data
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());

    }

    /**
     * Method for providing authentication => what is user allowed to do
     *
     * @param http parameter of http requests which allow to handle URLs of application
     * @throws Exception in case something went wrong
     */
    //Authorization=>what you allowed
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .headers().frameOptions().disable().and()
                .authorizeRequests()

                //Authorization for Admin
                .antMatchers("/admin/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET).permitAll()
                .antMatchers(HttpMethod.POST).permitAll()
                .antMatchers("/**").permitAll()

                .anyRequest().authenticated()

                //Providing login page, default success page and fail page

                .and()
                .formLogin().loginPage("/loginPage")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/")
                .failureUrl("/login-error");

    }

}
