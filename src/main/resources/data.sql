INSERT INTO recipes (id,creation_date,last_modification_date,author, full_text, pretext,title)
VALUES (1,null ,null ,null, 'To make Mocha coffee at home, you will need 1 cup hot brewed coffee,1 tablespoon unsweetened cocoa powder,1 tablespoon white sugar,2 tablespoons milk. Mix it up all together and enjoy your drink!',
 'Mocha coffee is extremely easy to make in this recipe. Just add cocoa, sugar and milk to hot coffee and drink!', 'Mocha coffee');

INSERT INTO recipes (id,creation_date,last_modification_date,author, full_text, pretext,title)
VALUES (2,null ,null ,null, 'Heat milk in a saucepan set over medium-low heat. Whisk briskly with a wire whisk to create foam. Brew espresso and pour into 4 cups. Pour in milk, holding back the foam with a spoon. Spoon foam over the top.',
 'Cafe Latte is a favourite drink of most of actors. Would you try it? ', 'Cafe Latte');

/*User with authority "ADMIN", username: admin,  password: 123 */
INSERT INTO app_user (id,creation_date,last_modification_date, password, username)
VALUES (1,null  ,null, '$2a$10$HyspApVYoX/R.hULZCDHieqZMYa.RSLOdYyYeZaDBNE2RWUEGZgKS', 'admin');
INSERT INTO role (role_id,role_name )VALUES(1,'ADMIN');
INSERT INTO app_user_roles (app_user_id,roles_role_id )VALUES(1,1);




